Tenta-se minimizar a espessura dos objetos utilizados.

Um �man (quadrado) de dimensoes na ordem dos mm (10^-3).
Magnetoresistencias na ordem dos um (10^-6)

-> Queremos linearizar a resposta (d� jeito) na nossa zona de trabalho.
-> N�o queremos saturar o sensor magnetoresistivo 
-> Minimizar a grossura da "pele" ao m�ximo
-> Tens�es geradas detect�veis por um sensor
-> Poder detetar diferentes tipos de for�a aplicada
-> Maximizar o numero de imans por unidade de �res

Para isso temos de dimensionar varios par�metros.

-> Escolha das propriedades do iman e distancia ao sensor (aka altura da camada de pele, campo pode ser 
demasiado grande e automaticamente satura a magnetoresistencia tem de existir equilibrio entre pot�ncia 
do iman e altura do mesmo). 
-> Escolha das propriedades do sensor ( rsquared, current, 
-> Coloca��o do sensor para a esquerda e direita do eixo do iman. (permite inserir mais que um sensor
por por iman, detetar diferentes tipos de for�a aplicados )
-> Varia��o de altura do iman com for�a aplicada (bulk modulus dos materiais utilizados)
-> Escolha das dist�ncias entre �mans adjacentes para fazer "mapa"


--- Dist�ncia do �man ao sensor ---
Quanto mais afastado estiver o iman do sensor mais linear � a resposta do mesmo,
porque o argumento do logaritmo � aproximadamente 1 e o mesmo pode ser aproximado em primeira ordem a uma reta.
Por isto 

--- Range de for�as detetadas pelo sensor --
Se tivermos uma esponja l espessura a compressibilidade da mesma em rela��o � press�o aplicada
pode ser dada por

l/k = dl/dP 

dP, se exercermos o equivalente a 1 kg (max) ou 10 g (min) na ponta de um dedo
a mesma possui uma �rea na ordem dos mmxmm (10^-6) portanto
deltaP = F/A ~= F/10^-6 = 10^7 Pa (max) ou 10^5 Pa (min)

k, se for borracha � na ordem dos
k ~= 10^9 Pa

Logo 

dl = l*dP/k
dlmax = l * 10^-2 ( compress�o � duas ordens de grandeza abaixo da grossura )
dlmin = l * 10^-4 ( compress�o � quatro ordens de grandeza abaixo da grossura )

Se assumirmos um pele de grossura l = 1mm ent�o a m�xima compress�o estar� na ordem do 10^-2  mm.

--------------------------------------------------------------------------------------

There can be two types of forces applied. One perpendicular to the sensor surface or 
parallel to it. If the magnet is placed according to fig . XXX 
