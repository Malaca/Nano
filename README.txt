﻿Fundamentação teórica (fórmulas, etc.)
Aplicação prática (como se constrói, etc.)
Visão de Mercado (está/pode ser vendido)


Describe why the technology is important and where can it be found (or not) in commercial
products.

Applications

- Substitute for natural limbs and phrosthetics ( still to far )
- When there are serious damages or burns the human skin can be replaced by artificial skin
  ( still to far )
- Application in robotics making them abble to sense pressure, touch, moisture, 
  temperature and proximity to objects. This way they will be abble to perform more
  delicate ande precise functions not yet achievable ( already reachable )
- Biomedial Instrumentation like measurement of the heart's electrical activity, brain waves, 
  muscle activity and other vital signals ( already reachable )
- Used for cosmetic products testing, removing the necessity of doing it on animals 
- Improve studies on UV-effects on the skin (penetration, premature photoaging, skin cancer,
  etc...) to designe better sun-screen protection

Current Technology

- Park, Tactile direction sensitive and stretchable electronic skins based on skins based 
  on human skin inspired interlocked microstructures
  Based on piezoresistive sensing principle created an e-skin capable of differentiating 
  a broad range of applied mechanic stimuly, including normal, shear, stretch, bend and
  torsion forces. It was also able of multidirectional force detection. By developing e-skin
  with 3x3 pixel arrays it was able to resolve the spatial distributions and directions of 
  various external stimuli. It was ables to distinguish between finger touches, air flows 
  and vibrations in terms of magnitude and direction ( ver artigo emerging flexible .... 
  pag. 11) 
- Harada, Fully printed flexible fingerprint-like three axis tactile and slip force and
  temperature sensors for artificial skin
  3x3 array of fingerprint like structures sandwiched between the arrays of temperature and
  strained sensors. It was capables of detecting the difference between finger touching, 
  finger slip/friction, N2 gas flow based on the distinct force meausured and the
  temperature mappings. ( ver artigo emerging flexible .... pag. 11) 
- Kim, Stretchable silicon nanoribbon electronics for skin prosthesis
  Smart prosthetic skin based on ultrathin silicon nanoribbons was developed for pressure,
  strain and temperature sensing. It had also humidity sensors included making it more 
  similar to biological skin. ( ver artigo emerging flexible .... pag. 13) 
- gmr_flexibleskin 
  Magnetic field sensor capable of detecting magnetic fields of hundredes of Oerst. Using
  a GMR sensor implanted on a strechable surface it's possible to visualise the magnetic
  field of an iman by the change in the resistance of the sensor. 


Theoretical background and principles of operation, describe physical/chemical principals of
operation, providing numbers and order of magnitude of the parameters detected/controlled

Basically there are 3 types of Sensing Platforms/Mechanims

Solid State Physical Sensing Platforms

- Strechable and wearable solid state sensors are made of polymers, carbon and metallic 
  conductor based  nanomaterials. Ex : polymer nanofibres, silver and gold nanoparticles
  and nanowires, CNTs (carbon nanotubes) and graphene.
- Incorporate solid-state components as their active sensing element.
- Suitable for flexible conductors because of high aspect ratio, superior electrical 
  conductivity and mechanical strength, low density
-  These hybrid structures 


Liquid State Physical Sensing Platforms

Mechanical Deformation Based Sensing Mechanisms

Design and modelling of a functional device based on the technology/theoretical principles
talked above. Include dimensions, geometry, structure, materials, methods of fabrication,
integration difficulties/challenges



Conclusions
